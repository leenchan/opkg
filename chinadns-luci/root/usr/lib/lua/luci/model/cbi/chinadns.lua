local running=(luci.sys.call("pidof chinadns > /dev/null") == 0)

if running then	
	m = Map("chinadns", translate("ChinaDNS"), translate("ChinaDNS is running"))
else
	m = Map("chinadns", translate("ChinaDNS"), translate("ChinaDNS is not running"))
end

s = m:section(TypedSection, "chinadns", translate("General Settings"))
s.anonymous = true

enabled = s:option(Flag, "enabled", translate("Enable"))
enabled.default = "0"
enabled.rmempty = false

mode = s:option(ListValue, "mode", translate("Mode"))
mode:value("0", translate("Normal"))
mode:value("1", translate("Dnsmasq Server"))
mode:value("2", translate("Specify Domains"))
mode:value("3", translate("UDP Forward"))
mode.default = "1"
mode.rmempty = false

tcp_forward = s:option(Value, "tcp_forward", translate("TCP Forward"), translate("Local TCP port or remote DNS server"))
tcp_forward:depends("mode", "3")

dns_server = s:option(Value, "dns_server", translate("DNS Servers"), translate("Separate with \",\" / blank for default"))

local_port = s:option(Value, "local_port", translate("Local Port"))
local_port.datatype = "port"
local_port.default = "5353"
local_port.rmempty = false

delay_time = s:option(Value, "delay_time", translate("Delay Time"),translate("Resault delay, default: 0.3 / max: 1 (sec)"))
delay_time.datatype = "range(0,1)"
delay_time.default = "0.3"
delay_time.rmempty = false

fake_ip_list_enabled = s:option(Flag, "fake_ip_list_enabled", translate("Fake IP List"), "/etc/ipset/fake")
fake_ip_list_enabled.default = "1"
fake_ip_list_enabled.rmempty = false

china_ip_list_enabled = s:option(Flag, "china_ip_list_enabled", translate("China IP List"), "/etc/ipset/china")
china_ip_list_enabled.default = "1"
china_ip_list_enabled.rmempty = false

mutation = s:option(Flag, "compression", translate("Compression Pointer"), "Using DNS compression pointer mutation")
mutation.default = "1"
mutation.rmempty = false

bidirectional = s:option(Flag, "bidirectional", translate("Bidirectional Filter"), translate("Also filter results inside China from foreign DNS servers"))
bidirectional.default = "1"
bidirectional.rmempty = false

return m
