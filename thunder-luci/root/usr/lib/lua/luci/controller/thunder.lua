
module("luci.controller.thunder", package.seeall)

function index()
	if not nixio.fs.access("/etc/config/thunder") then
		return
	end

	local page
	page = entry({"admin", "services", "thunder"}, cbi("thunder"), _("Xware"), 56)
	page.i18n = "thunder"
	page.dependent = true
end
