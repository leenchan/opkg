local fs = require "nixio.fs"

local running=(luci.sys.call("pidof adbyby > /dev/null") == 0)
if running then	
	m = Map("adbyby", translate("ADBYBY"), translate("ADBYBY is running"))
else
	m = Map("adbyby", translate("ADBYBY"), translate("ADBYBY is not running"))
end

local adbyby_path = tostring(m.uci:get( "adbyby", "config", "path" ))
local adbyby_user = adbyby_path:gsub("%a+$", "data/user.txt")

s = m:section(TypedSection, "adbyby", translate("Settings"))
s.anonymous = true

general = s:tab("general", translate("General"))

enabled = s:taboption("general", Flag, "enabled", translate("Enable"))
enabled.default = '0'
enabled.rmempty = false

path = s:taboption("general", Value, "path", translate("Path"))
path.default = "/usr/bin/adbyby"
path.rmempty = false

user_rules = s:tab("editconf_user", translate("User Rules"))
editconf_user = s:taboption("editconf_user", Value, "_editconf_user", "", translate("Comment by \"!\""))
editconf_user.template = "cbi/tvalue"
editconf_user.rows = 20
editconf_user.wrap = "off"
function editconf_user.cfgvalue(self, section)
	return fs.readfile(adbyby_user) or ""
end
function editconf_user.write(self, section, value)
	if value then
		value = value:gsub("\r\n?", "\n")
		fs.writefile(adbyby_user, value)
	end
end

return m
