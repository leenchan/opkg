local running=(luci.sys.call("pidof dnscrypt-proxy > /dev/null") == 0)

if running then	
	m = Map("dnscrypt-proxy", translate("DNSCrypt-Proxy"), translate("DNSCrypt-Proxy is running"))
else
	m = Map("dnscrypt-proxy", translate("DNSCrypt-Proxy"), translate("DNSCrypt-Proxy is not running"))
end

s = m:section(TypedSection, "dnscrypt-proxy", translate("DNSCrypt-Proxy"))
s.anonymous = true

enable = s:option(Flag, "enabled", translate("Enable"))
enable.rmempty = false

mode = s:option(ListValue, "mode", translate("Mode"))
mode:value("0", translate("Normal"))
mode:value("1", translate("Dnsmasq Server"))
mode:value("2", translate("Specify Domains"))
mode.default = "0"
mode.rmempty = false

local_address = s:option(Value, "local_address", translate("Local Address"),translate("Default is 127.0.0.1"))
local_address.default = "127.0.0.1"
local_address.datatype = "ipaddr"
local_address.rmempty = false

local_port = s:option(Value, "local_port", translate("Local Port"), translate("Default is 2053"))
local_port.default = "2053"
local_port.datatype = "port"
local_port.rmempty = false

resolvers_list = s:option(Value, "resolvers_list", translate("Resolvers list"))
resolvers_list.default = "/usr/share/dnscrypt-proxy/dnscrypt-resolvers.csv"
resolvers_list.rmempty = false

resolver = s:option(Value, "resolver", translate("Resolver"))
resolver.default = "d0wn-sg-ns1"
resolver.rmempty = false

return m
