m = Map("overthewall", translate("OverTheWall"))
m.anonymous = true

update = m:section(TypedSection, "overthewall", translate("Update Files"))
update.anonymous = true

update:tab("general", translate("General"))

auto_update = update:taboption("general", ListValue, "auto_update", translate("Update Every"))
auto_update:value("0", translate("Disable"))
auto_update:value("* */12 * * *", translate("12 hours"))
auto_update:value("* * */1 * *", translate("day"))
auto_update:value("* * */3 * *", translate("3 days"))
auto_update:value("* * * * */1", translate("week"))
auto_update:value("* * * */0.5 *", translate("half month"))
auto_update:value("* * * */1 *", translate("month"))
auto_update:value("* * * */2 *", translate("2 months"))
auto_update:value("* * * */3 *", translate("3 months"))
auto_update:value("* * * */6 *", translate("half year"))
auto_update.default = "0"

php_url = update:taboption("general", Value, "php_url", translate("PHP URL"))
php_url.default = "http://leenchan.sourceforge.net/file/index.php"
php_url.rmempty = false

update_selected = update:taboption("general", Button, "update_selected", " ")
update_selected.inputtitle = translate("Update Selected Files")
update_selected.inputstyle = "apply"
function update_selected.write(self, section, value)
        update_selected.inputtitle = translate("Updating...")
        update.description = "<pre>"..luci.sys.exec("/etc/init.d/overthewall download_all").."</pre>"
        update_selected.inputtitle = translate("Update Selected Files")
end

restart_enabled = update:taboption("general", Flag, "restart_enabled", translate("Restart Services"))
restart_enabled.default = "0"
restart_enabled.rmempty = false

restart_services = update:taboption("general", Value, "restart_services", translate("Service Name"), translate("Restart services after update, separate with \",\""))

restart_services_now = update:taboption("general", Button, "restart_services_now", " ")
restart_services_now.inputtitle = translate("Restart Services")
restart_services_now.inputstyle = "apply"
function restart_services_now.write(self, section, value)
        restart_services_now.inputtitle = translate("Restarting...")
        update.description = "<pre>"..luci.sys.exec("/etc/init.d/overthewall restart_services_now").."</pre>"
        restart_services_now.inputtitle = translate("Restart Services")
end

update:tab("chinaip", translate("China IP List"))

chinaip_selected = update:taboption("chinaip", Flag, "chinaip_selected", translate("Selected"), "/etc/ipset/china")
chinaip_selected.default = "0"
chinaip_selected.rmempty = false

chinaip_mode = update:taboption("chinaip", ListValue, "chinaip_mode", translate("Source"))
chinaip_mode:value("0",translate("Plain URL"))
chinaip_mode:value("1",translate("PHP URL"))
chinaip_mode.default = "1"
chinaip_mode.rmempty = false

chinaip_plain_url = update:taboption("chinaip", Value, "chinaip_plain_url", translate("Plain URL"))
chinaip_plain_url.default = "http://ftp.apnic.net/apnic/stats/apnic/delegated-apnic-latest"
chinaip_plain_url:depends("chinaip_mode","0")
chinaip_plain_url.rmempty = false

chinaip_update = update:taboption("chinaip", Button, "chinaip_update", " ")
chinaip_update.inputtitle = translate("Update China IP List")
chinaip_update.inputstyle = "apply"
function chinaip_update.write(self, section, value)
        chinaip_update.inputtitle = translate("Updating...")
        update.description = "<pre>"..luci.sys.exec("/etc/init.d/overthewall download_china_ip_only").."</pre>"
        chinaip_update.inputtitle = translate("Update China IP List")
end

update:tab("fakeip", translate("Fake IP List"))

fakeip_selected = update:taboption("fakeip", Flag, "fakeip_selected", translate("Selected"), "/etc/ipset/fake")
fakeip_selected.default = "0"
fakeip_selected.rmempty = false

fakeip_mode = update:taboption("fakeip", ListValue, "fakeip_mode", translate("Source"))
fakeip_mode:value("0",translate("Plain URL"))
fakeip_mode:value("1",translate("PHP URL"))
fakeip_mode.default = "1"
fakeip_mode.rmempty = false

fakeip_plain_url = update:taboption("fakeip", Value, "fakeip_plain_url", translate("Plain URL"))
fakeip_plain_url.default = "https://raw.githubusercontent.com/clowwindy/ChinaDNS/master/iplist.txt"
fakeip_plain_url:depends("fakeip_mode","0")
fakeip_plain_url.rmempty = false

fakeip_update = update:taboption("fakeip", Button, "fakeip_update", " ")
fakeip_update.inputtitle = translate("Update Fake IP List")
fakeip_update.inputstyle = "apply"
function fakeip_update.write(self, section, value)
        fakeip_update.inputtitle = translate("Updating...")
        update.description = "<pre>"..luci.sys.exec("/etc/init.d/overthewall download_fake_ip_only").."</pre>"
        fakeip_update.inputtitle = translate("Update Fake IP List")
end

update:tab("gfwlist", translate("GFWList"))

gfwlist_selected = update:taboption("gfwlist", Flag, "gfwlist_selected", translate("Selected"), "/etc/ipset/gfwlist")
gfwlist_selected.default = "0"
gfwlist_selected.rmempty = false

gfwlist_mode = update:taboption("gfwlist", ListValue, "gfwlist_mode", translate("Source"))
gfwlist_mode:value("0",translate("Plain URL"))
gfwlist_mode:value("1",translate("PHP URL"))
gfwlist_mode.default = "1"
gfwlist_mode.rmempty = false

gfwlist_plain_url = update:taboption("gfwlist", Value, "gfwlist_plain_url", translate("Plain URL"))
gfwlist_plain_url.default = "http://autoproxy-gfwlist.googlecode.com/svn/trunk/gfwlist.txt"
gfwlist_plain_url:depends("gfwlist_mode","0")
gfwlist_plain_url.rmempty = false

gfwlist_update = update:taboption("gfwlist", Button, "gfwlist_update", " ")
gfwlist_update.inputtitle = translate("Update GFWList")
gfwlist_update.inputstyle = "apply"
function gfwlist_update.write(self, section, value)
        gfwlist_update.inputtitle = translate("Updating...")
        update.description = "<pre>"..luci.sys.exec("/etc/init.d/overthewall download_gfw_list_only").."</pre>"
        gfwlist_update.inputtitle = translate("Update GFWList")
end

return m
