
m = Map("n2n", translate("N2N VPN"))

edge = m:section(TypedSection, "edge", translate("N2N CLIENT"))
edge.addremove = true
edge.anonymous = false
edge:option(Flag, "enabled", translate("Enable"))
supernode_host = edge:option(Value, "supernode_host", translate("Supernode Host"))
supernode_host.rmempty = true
supernode_host.datatype = "host"
supernode_port = edge:option(Value, "supernode_port", translate("Supernode Port"))
supernode_port.rmempty = true
supernode_port.data = "port"
mode = edge:option(ListValue, "mode", translate("Mode"))
mode:value("static","Static")
mode:value("dhcp","DHCP")
mode.rmempty = true
ipaddress = edge:option(Value, "ipaddress", translate("IP Address"))
ipaddress.rmempty = true
ipaddress.datatype = "ipaddr"
ipaddress:depends("mode","static")
netmask = edge:option(Value, "netmask", translate("Netmask"))
netmask.default = "255.255.255.0"
netmask.rmempty = true
netmask.datatype = "ipaddr"
device_name = edge:option(Value, "device_name", translate("Device Name"))
device_name.rmempty = true
community = edge:option(Value, "community", translate("Group Name"))
community.rmempty = true
password = edge:option(Value, "key", translate("Password"))
password.rmempty = true
password.password = true
route = edge:option(Flag, "route", translate("Route"))
route.rmempty = true
route:depends("mode","static")

supernode = m:section(TypedSection, "supernode", translate("N2N SERVER"))
supernode.addremove = true
supernode.anonymous = false
supernode:option(Flag, "enabled", translate("Enable"))
supernode:option(Value, "port", translate("port")).rmempty = true

return m
