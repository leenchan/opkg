#
# Copyright (C) 2012-2014 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#
include $(TOPDIR)/rules.mk

PKG_NAME:=aria2
PKG_VERSION:=1.26.0
PKG_RELEASE:=1

PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_URL:=https://github.com/tatsuhiro-t/aria2/releases/download/release-$(PKG_VERSION)/
PKG_MD5SUM:=cafc17f0ca4229756ff379d20d428780
PKG_INSTALL:=1

PKG_MAINTAINER:=Imre Kaloz <kaloz@openwrt.org>
PKG_LICENSE:=GPLv2
PKG_LICENSE_FILES:=COPYING

include $(INCLUDE_DIR)/package.mk
include $(INCLUDE_DIR)/nls.mk

define Package/aria2
	SECTION:=net
	CATEGORY:=Network
	SUBMENU:=File Transfer
	TITLE:=lightweight download utility (full)
	URL:=https://github.com/tatsuhiro-t/aria2/
	DEPENDS:=+zlib +libstdcpp +libopenssl +libxml2
endef

define Package/aria2/description
aria2 is a lightweight multi-protocol & multi-source command-line download
utility
endef

CONFIGURE_ARGS += \
		--disable-nls \
		--enable-ssl \
		--with-openssl \
		--without-gnutls \
		--enable-bittorrent \
		--enable-metalink \
		--with-libxml2 \
		--without-libnettle \
		--without-libgmp \
		--without-libgcrypt \
		--without-libexpat \
		--without-libcares \
		--without-sqlite3 \
		--with-libz

define Package/aria2/install
		$(INSTALL_DIR) $(1)/usr/bin
		$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/aria2c $(1)/usr/bin
endef

$(eval $(call BuildPackage,aria2))
